﻿import java.util.Scanner;


class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       
      while(true) {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double tickets;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       eingezahlterGesamtbetrag = fahrkartenBezahlen (zuZahlenderBetrag);
       fahrkartenAusgeben();
       warte();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);}
      }
    

     
	public static double fahrkartenbestellungErfassen () {
	double zuZahlenderBetrag; 
	double tickets;
	//mehrschichtiger Array statt scanner
	
	
	
	Scanner tastatur = new Scanner(System.in);
	System.out.print("Zu zahlender Betrag (EURO): ");
	String [] kartenArray = new String [10];
	float [] preisArray = new float [10];
	
	
	
	
    zuZahlenderBetrag = tastatur.nextDouble();
    // Eingabe Ticketanzahl und multiplikation mit Betrag
    System.out.print("Anzahl der Tickets: ");
    tickets = tastatur.nextDouble();
    if (tickets > 1 && 10 > tickets) {
    zuZahlenderBetrag = zuZahlenderBetrag * tickets;
    //tastatur.close(); Scanner muss auf sein
	}
    else {
    	tickets = 1; System.out.println("Die mögliche Ticketanzahl liegt zwischen 1 und 10. Es wird mit einem Ticket weiter gerechnet");
    }
    return zuZahlenderBetrag;
    }
	
	
	public static double fahrkartenBezahlen (double zuZahlenderBetrag) {
	       // Geldeinwurf
	       // -----------
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		Scanner tastatur = new Scanner(System.in);
	    eingezahlterGesamtbetrag = 0.0;
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	    {
	    	System.out.print("Noch zu zahlen: " );
	    	System.out.printf("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	System.out.print(" Euro\n");
	    	System.out.print("Eingabe (mind. 5 Ct, höchstens 2 Euro): ");
	    	//eingeworfeneMünze = 3.0; test für scanner
	    	eingeworfeneMünze = tastatur.nextDouble();
	        eingezahlterGesamtbetrag += eingeworfeneMünze;
	        
	             
	     }
	    //tastatur.close(); muss geschlossen bleiben
	    return eingezahlterGesamtbetrag;
	     }
	
	public static void fahrkartenAusgeben () {
	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       }
	
	public static void warte() {
		for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {  // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       double rückgabebetrag; 
			rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.print("Der Rückgabebetrag in Höhe von " );
	    	   System.out.printf("%.2f", rückgabebetrag);
	    	   System.out.print(" Euro");
	    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	   
	        	  System.out.println("2 Euro");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 Euro");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n\n");
	       return 0;
	    }
	    
}