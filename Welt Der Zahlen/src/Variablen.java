package eingabeAusgabe;

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	short zaehler;
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	zaehler = 25;
	System.out.println(zaehler);
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	char eingabe;
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	eingabe = 'C';
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	long astroWert;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	int vLicht = 300000; 
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	byte anzahlMitglieder;
    /* 8. Geben Sie die Anzahl der Mitgl,ieder auf dem Bildschirm aus.*/
	System.out.println(anzahlMitglieder);
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	double elementarladung = 1.602176634E-19;
	System.out.println("Elementarladung" + elementarladung);
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	boolean zahlung;
	
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	zahlung = true;
	
	if (zahlung == true) {
		System.out.println("Zahlung ist erfolgt.");
		}
	
	else (zahlung == false) {
		System.out.println("Zahlung ist nicht erfolgt.");
		}
		
	
	
	
	}
  }//main
// Variablen