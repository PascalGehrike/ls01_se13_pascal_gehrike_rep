/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 25000000000L;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 8760;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44F; 
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten + "\n");
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne + "\n");

    System.out.println("Anzahl der Bewohner Berlins: " + bewohnerBerlin + "\n");

    System.out.println("Anzahl der Tage meines Alters: " + alterTage + "\n");

    System.out.println("Gewichts des gr��ten Tieres: " + gewichtKilogramm + "\n");

    System.out.println("Fl�che des gr��ten Landes: " + flaecheGroessteLand + "\n");

    System.out.println("Fl�che des kleinsten Landes: " + flaecheKleinsteLand + "\n" + "\n" + "\n" + "\n");
    
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

